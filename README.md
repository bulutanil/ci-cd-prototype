# CI-CD Prototype

This project is a prototype of using CI / CD processes in GitLab.
Uses CMake to  compile, Cppcheck for static code analysis and Google Test for unit tests.


## About Project

This repository contains a simple prototype project on how to do CI / CD processes on Gitlab. The repository consists of two parts. In the Prototype project part there is the actual code that will go to production. There is also the PrototypeProjectTest section, which is triggered by the change commit in PrototypeProject. It is checked whether the change is consistent with the unit test and mock functions available here.

The prototype project is quite simple. It is assumed that the C++ code connects to and gets a number from a database. Afterwards, the factorial value of this number is calculated and rewritten into the database. 

## Details
Some features and libraries used in the project are described below.

**Gitlab Pipeline:** It is used for build and test processes. As it appears in the ".yml" file, the operations to be performed over the scripts are defined. After the push operation, both the real project and the test project are compiled first. Static code analysis and unit test processes defined later are executed in parallel. The realization of the static code analysis and unit test process depends on the successful execution of the build process.

**CMake:** It has been used to carry out the compilation process of the projects automatically, not through IDE. All dependencies between libraries are defined through CMake.

*The project is designed to run on Windows OS and needs the MinGW compiler by default. Therefore, the "MinGW/bin" path must be specified in the CMake file. If you want to update this path or use another compiler, the "set(CMAKE_CXX_COMPILER C:/MinGW/bin" command in the CMake file must be updated. 

**Cppcheck:**  It is used for static code analysis. It is an open source tool.

https://cppcheck.sourceforge.io/

https://github.com/danmar/cppcheck

The .exe file of version 2.6 is used. It can be changed at will.

**Google Test:** It is an open source and very capable unit testing library. Factorial values calculated in this project were used in operations related to database and database.

https://github.com/google/googletest

**Google Mock:** It is an open source library. It is used to simulate all or certain parts of the code and behave as desired. It is used for database related operations in this project. Functions that return the desired result are implemented as if there is a really working database.

https://github.com/google/googletest



```

