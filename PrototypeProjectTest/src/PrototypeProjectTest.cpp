//============================================================================
// Name        : PrototypeProjectTest.cpp
// Author      : Anil Bulut
// Version     :
//============================================================================
#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include "FactorialTest.cpp"
#include <IDBOperations.h>
#include "MockOperations.cpp"
#include <iostream>

using namespace std;

/**
*@brief This syntax is used for running GTest.
* You can use the following options while running the tests.
* @code 
* ./PrototypeProjectTest  //all tests run without any flags
* ./PrototypeProjectTest --gest_filter = FactorialTest.* //run all the tests in FactorialTest category
* ./PrototypeProjectTest --gest_filter = FactorialTest.*:-FunctionalityTest_5Factorial //run all the tests in FactorialTest category except 5 fact.
* ./PrototypeProjectTest --gtest_repeat = 1000 // Run 1000 times and don't stop on errors
* ./PrototypeProjectTest --gtest_repeat = -1 //  Works infinitely for negative numbers
* ./PrototypeProjectTest --gtest_repeat = 1000 --gtest_break_on_failure // Run 1000 times but stop if assert happens
* ./PrototypeProjectTest --gtest_repeat = 1000 --gest_filter = FactorialTest.* // Run FactorialTest category tests 1000 times
* ./PrototypeProjectTest --gtest_output ='xml:path_to_output" // write test results to xml in the specified path
* ./PrototypeProjectTest --gtest_output ='json:path_to_output" // write test results to json in the specified path
* @endcode
*/

int main(int argc, char** argv) {
	::testing::InitGoogleMock(&argc, argv); // mock before
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
