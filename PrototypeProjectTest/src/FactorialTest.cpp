#include "Factorial.h"
#include <gtest/gtest.h>

class FactorialTest: public ::testing::Test, public Factorial
{
public:
    Factorial* m_fact;
protected:
    virtual void SetUp()
    {
    }

    virtual void TearDown()
    {
        
    }

};

/**
* @brief In this class, only the factorial calculation function is tested independent of database operations.
* FactorialTest stands for category. In the next stages, options such as run only the tests in this category may be appropriate.
* FunctionalityTest_5Factorial stands for test name.
* EXPECT_EQ statement controls that whether the result equals to expected value. But it is not fatal. Tests can continue even if result is wrong.
*/
TEST_F(FactorialTest,FunctionalityTest_5Factorial)
{
	//act
	int result = m_fact->useCalculationOfFactorial(m_fact,5);
	//assert
	EXPECT_EQ(result,120);
}
/**
* @brief FactorialTest stands for category. In the next stages, options such as run only the tests in this category may be appropriate.
* FunctionalityFunctionalityTest_0 stands for test name.
* EXPECT_EQ statement controls that whether the result equals to expected value. But it is not fatal. Tests can continue even if result is wrong.
*/
TEST_F(FactorialTest,FunctionalityTest_0Factorial)
{
    Factorial* m_fact;
	//act
	int result = m_fact->useCalculationOfFactorial(m_fact,0);
	//assert
	EXPECT_EQ(result,1);
}
/**
* @brief FactorialTest stands for category. In the next stages, options such as run only the tests in this category may be appropriate.
* FunctionalityTest_NegativeFactorialstands for test name.
* EXPECT_EQ statement controls that whether the result equals to expected value. But it is not fatal. Tests can continue even if result is wrong.
*/
TEST_F(FactorialTest,FunctionalityTest_NegativeFactorial)
{
    Factorial* m_fact;
	//act
	int result = m_fact->useCalculationOfFactorial(m_fact,-10);
	//assert
	EXPECT_EQ(result,-1);
}
