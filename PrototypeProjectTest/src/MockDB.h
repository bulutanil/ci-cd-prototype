#include <IDBOperations.h>
#include <gmock/gmock.h>

/**
 * @brief This class contains mock formats of functions that the Prototype Project needs but has no real implementation.
 * The class is inherited from the header file, which contains the actual function signatures in the Prototype Project.
 * In this way, it can be used where the IDBOperations object is needed.
 */
class MockDB: public IDBOperations
{
public:
    /**
     * @brief It is the mock version of the login function found in IDBOperations.h.
     * @param The expression "bool" indicates the return type of the function. The expression "login" is the name of the function.
     * The expressions "username" and "password" are parameters that must be given to the function.
     * Finally, the expression "override" indicates that it will replace the actual login function.
     */
	MOCK_METHOD(bool, login, (string username, string password), (override));
	MOCK_METHOD(bool, logout,(string username),(override));
	MOCK_METHOD(int, getNumberFromDB, (), (override));
	MOCK_METHOD(bool, writeNumberToDB,(int input), (override));
};
