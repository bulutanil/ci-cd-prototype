#include <iostream>
#include <cstdlib>
#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <DBOperations.h>
#include "MockDB.h"

/**
 * @brief In this class, database-related functions are tested.
 */

using ::testing::Return;
using ::testing::AllOf;
using ::testing::Ge;
using ::testing::Le;

/**
* @brief It is used to generate random numbers that will be taken from the database and calculated the factorial value.
* @param To determine the range of the random number to be generated, lower and upper values should be given.
* @return The random number is returned which is generated under the desired conditions.
*/
int generateRandomNumber(int minRange, int maxRange)
{
    srand(time(0));
    return minRange + (rand() % (maxRange - minRange +1));
}

/**
* @brief It is the block where the login process is tested.
*/

TEST(DBTest,LoginTest)
{
    //Arrange
    MockDB mdb;
    DBOperations db(&mdb);
    EXPECT_CALL(mdb, login("MyName", "MyPassword")).Times(1).WillOnce(Return(true));
    //Act
    bool retValue = db.init("MyName", "MyPassword");
    //Assert
    EXPECT_EQ(retValue,true);
}
/**
* @brief It is the block where the data is taken from the database, the factorial is calculated and the process of writing to the database is tested.
 * In the prototype project, the mock version of the login function has also been used again, since it is necessary to login first to perform these operations.
 * As future work, it can be considered to move the information that the previous test block was logged in to this test block by writing test fixture.(TEST_F)
 * Since the part to be tested here is the database-related operations, the factorial part is only a formality.
 * For this reason, a random input was given to the factorial calculation process and all possible values were accepted as valid.
 * Test blocks that test factorial operations are available in the Factorial.cpp file.
*/

TEST(DBTest,DoJobTest)
{
    //Arrange
    MockDB mdb;
    DBOperations db(&mdb);
    EXPECT_CALL(mdb, login("MyName", "MyPassword")).Times(1).WillOnce(Return(true));
    EXPECT_CALL(mdb, getNumberFromDB()).Times(1).WillOnce(Return(generateRandomNumber(0,10)));
    EXPECT_CALL(mdb, writeNumberToDB(AllOf(Ge(-1),Le(INT_MAX)))).Times(1).WillOnce(Return(true));
    //Act
    db.init("MyName", "MyPassword");
    bool retValue = db.doJob();
    //Assert
    EXPECT_EQ(retValue,true);
}
/**
* @brief It is the block where the logout process is tested.
*/
TEST(DBTest,LogOutTest)
{
    //Arrange
    MockDB mdb;
    DBOperations db(&mdb);
    EXPECT_CALL(mdb, logout("MyName")).Times(1).WillOnce(Return(true));
    //Act
    bool retValue = db.disconnect("MyName");
    //Assert
    ASSERT_EQ(retValue,true);
}
/**
* @brief In this block, calculating without logging in has been tested.
 * However, if the code of the prototype project is examined, it is a prerequisite to login operation to retrieve data from the database and write it back.
 * Since these operations cannot be performed without login, false value should be returned.
*/

TEST(ExceptionalTest,DoJobWithoutLogin)
{
     //Arrange
    MockDB mdb;
    DBOperations db(&mdb);
    //Act
    bool retValue = db.doJob();
    //Assert
    EXPECT_EQ(retValue,false);
}
