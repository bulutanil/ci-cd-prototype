/*
 * DBOperations.h
 *
 *  Created on: 4 Ara 2021
 *      Author: Anil
 */

#ifndef SRC_DBOPERATIONS_H_
#define SRC_DBOPERATIONS_H_

#include "IDBOperations.h"
#include "Factorial.h"
#include <iostream>

using namespace std;

/**
 * @brief It will be attempted to connect to the database using functions that have not yet been implemented in this class.
 * The functions in this class, will call the functions that exists in IDBOperations.h interface.
 */
class DBOperations{

public:
    /**
     * @brief The constructor normally needs the original IDBOperations object to connect to the database.
     * But in this case the functions in IDBOperations are not implemented.
     * In the test project, a mock class inherited from IDBOperations will be generated and the object of that mock class will be given to the constructor.
     */
	DBOperations(IDBOperations* a_db);
	virtual ~DBOperations();

	/**
	 * @brief This function tries to login
	 * @param login username
	 * @param login password
	 * @return whether the login operation is successful accordıng to gıven username and password.
	 */
	bool init(string username, string password);
	/**
	 * @brief This function tries to logout
	 * @param username will be used for logout of specific user.
	 * @return whether the logout operation is successful accordıng to gıven username.
	 */
	bool disconnect(string username);
	/**
	 * @return wReturns true if getting number from the database, calculating the factorial value of that number, and then writing the calculated value to the database are all complete.
	 */
	bool doJob();



private:
	IDBOperations* m_db;
	Factorial* m_fact;
	bool m_loginStatus;
	bool m_numberGetStatus;
	int m_number;

};

#endif /* SRC_DBOPERATIONS_H_ */
