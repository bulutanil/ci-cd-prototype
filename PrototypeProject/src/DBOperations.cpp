/*
 * DBOperations.cpp
 *
 *  Created on: 4 Ara 2021
 *      Author: Anil
 */

#include "DBOperations.h"

DBOperations::DBOperations(IDBOperations* a_db)
{
	m_db = a_db;
	m_fact = new Factorial();
	m_loginStatus = false;
	m_numberGetStatus = false;
}

DBOperations::~DBOperations() 
{
	
}

bool DBOperations::init(string username, string password)
{

	if(m_db->login(username,password) != true)
	{
		cout << "Login failed!" << endl;
		m_loginStatus = false;
		return false;
	}
	else
	{
		cout << "Login Successful!" << endl;
		m_loginStatus = true;
		return true;
	}	
}
bool DBOperations::disconnect(string username)
{
	if(m_db->logout(username) != true)
	{
		cout << "Problem occured during disconnection!" << endl;
		return false;
	}
	else
	{
		cout << "Successfully disconnected!" << endl;
		return true;
	}
}
bool DBOperations::doJob()
{
	if(m_loginStatus)
	{
		m_number = m_db->getNumberFromDB();
		m_numberGetStatus = true;
		m_fact->calculateFactorial(m_number);
		if(m_db->writeNumberToDB(m_number))
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}
	else
	{
		m_numberGetStatus = false;
		return false;
	}
}


