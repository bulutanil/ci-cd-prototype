/*
 * Factorial.h
 *
 *  Created on: 4 Ara 2021
 *      Author: Anil Bulut test
 */

#ifndef SRC_FACTORIAL_H_
#define SRC_FACTORIAL_H_

/**
 * @brief This class recursively calculates the factorial value of the incoming input.
 * Factorial values of large numbers may not fit integer values and overflow situations may occur.
 * @return Returns -1 for negative numbers.
 */
class DBOperations;

class Factorial {
public:

    /**
     * @brief This declaration is made to use calculateFactorial() method from DPOperations.
     */
    friend class DBOperations;

	Factorial();
	virtual ~Factorial();
protected:
	int calculateFactorial(int input);

    /**
     * @brief This function is caused by the ridiculous rules of C++.
     * Normally base class' protected functions should be used in child classes. But C++ prevents this from being used directly and forces it to get around it below way.
     * https://stackoverflow.com/a/30111934
     * Just to test the factorial class, I just wanted to create a class(FactorialTest.cpp) in the test project and inherit it from Factorial.h. In this way, I would be able to use the calculateFactorial() function, which is protected in Factorial.h.
     * Note to Myself: Learn higher-level languages better and save yourself from C++. People can create projects using only annotations.
     * @param  fact = class' self reference. I'd be kidding if I saw someone else do this. But I had to.
     */
    static int useCalculationOfFactorial( Factorial* fact, int input )
    {
        return fact->calculateFactorial(input);
    }
};

#endif /* SRC_FACTORIAL_H_ */
