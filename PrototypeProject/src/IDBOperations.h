/*
 * IDBOperations.h
 *
 *  Created on: 4 Ara 2021
 *      Author: Anil
 */

#ifndef SRC_IDBOPERATIONS_H_
#define SRC_IDBOPERATIONS_H_
#include <string>

using namespace std;

/*
 * @brief This class is an interface that contains the signatures of the functions to be used to connect to the database.
 * We can think that another team should write the functions according to the template here, but they haven't written them yet.
 * However, we have written our own code using these functions in the DBOperations.cpp class that we are trying to connect to the database and we want to test it.
 * Therefore, a mock class inherited from this class will be written in the test project.
 * And functions in the DBOperations.cpp will be tested as if these functions were implemented.
 */
class IDBOperations {
public:
	virtual ~IDBOperations() {};


	virtual bool login(string username, string password) = 0;
	virtual bool logout(string username) = 0;
	virtual int getNumberFromDB() = 0;
	virtual bool writeNumberToDB(int input) = 0;
};

#endif /* SRC_IDBOPERATIONS_H_ */
