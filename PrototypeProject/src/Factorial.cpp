/*
 * Factorial.cpp
 *
 *  Created on: 4 Ara 2021
 *      Author: Anil Bulut
 */

#include "Factorial.h"
Factorial::Factorial() 
{

}

Factorial::~Factorial() {
	
}

int Factorial::calculateFactorial(int input)
{
    if(input < 0)
    {
        return -1;
    }
    else if(input <= 1)
    {
        return 1;
    }

    int res = 1, i;
    for (i = 2; i <= input; i++)
        res *= i;
    return res;
}


